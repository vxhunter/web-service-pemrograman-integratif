# E-VOTE

Merupakan sebuah web service yang berguna dalam menangani HTTP Method GET dan PATCH dari HTTP request & HTTP response antar Web Apllication e-Vote dengan server database.  
Web service ini juga berfungsi sebagai penghubung pengiriman data berformat JSON antara web app dan server database.  
  
Laman e-Vote dapat diakses melalui [link](http://167.205.67.247:6969) ini.  
(Harus terkoneksi dengan VPN ITB atau berada dalam lingkup jaringan ITB)

## Author
Powered by Aries Adjie Pangestu  
18215033  
Version v1.0

## System Requirements

System requirements web service ini dapat dilihat secara lengkap pada folder **documents**.

## API

Berikut ditampilkan API yang digunakan dalam web service ini.  
```html
> https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Open+Sans:300,400,700,800
> ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js
```
## How it Works
Rancangan bagaimana web service ini dapat bekerja, dapat dilihat pada foler **model & design**.  
  
Berikut NIK yang terdaftar dalam database:

* 3204320404970017
* 3204320504990001
* 3204322112900021
* 3204320411920008
* 3204320408980008
* 3204320409870008
* 3204320412900008
* 3204320401870008
* 3204320401790008
* 3204320405990008

## Testing
Hasil testing dapat dilihat pada folder **testing**.
