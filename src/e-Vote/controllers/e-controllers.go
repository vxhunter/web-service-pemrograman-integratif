package controllers

import (
  "fmt"
  "log"
  "net/http"
  "encoding/json"
  "database/sql"
  "io"
  _ "github.com/go-sql-driver/mysql"
  "strconv"
  "e-Vote/models"
)

func GetAllUser(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/data_voting")

	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()

	voting := models.DataVoting{};

	rows, err := db.Query("SELECT id,nik,nama,pilihan from voting")
	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()

	for rows.Next() {
		err := rows.Scan(&voting.ID, &voting.NIK, &voting.Nama, &voting.Pilihan)
		if err!= nil {
			log.Fatal(err)
		}

		json.NewEncoder(w).Encode(&voting);
	}
	err = rows.Err()

}

func GetUser(w http.ResponseWriter, r *http.Request, id string) {
	myid, _ := strconv.Atoi(id)

	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/data_voting")

	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()

	voting := models.DataVoting{};

	rows, err := db.Query("select * from voting WHERE id = ?", myid)

	if err!= nil {
		log.Fatal(err)
	}

	defer db.Close()

	for rows.Next() {
		err := rows.Scan(&voting.ID, &voting.NIK, &voting.Nama, &voting.Pilihan)
		if err!= nil {
			log.Fatal(err)
		}

		json.NewEncoder(w).Encode(&voting);
	}
	err = rows.Err()
}


func PatchPenduduk(w http.ResponseWriter, r *http.Request) {
	out := make([]byte, 1024)

	bodyLen, err := r.Body.Read(out)

	if err != io.EOF {
		fmt.Printf(err.Error())
		w.Write([]byte("{error:" + err.Error() + "}"))
		return
	}
	var k models.DataVoting
	err = json.Unmarshal(out[:bodyLen],&k)
	if err != nil {
		w.Write([]byte("{error:" + err.Error() + "}"))
		return
	}
	idx := insertInDatabase(k)
	log.Printf("ID Baru %d",idx)
}

func insertInDatabase(data models.DataVoting) int64 {
	log.Printf(data.Nama)
	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/data_voting")

	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()

	result, err := db.Exec("UPDATE voting SET Pilihan=? WHERE ID=?", 1, data.ID)

	if err != nil {
		log.Fatal(err)
	}
  
	lastid,_ := result.LastInsertId();
	return lastid
}
