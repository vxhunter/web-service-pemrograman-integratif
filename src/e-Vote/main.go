package main

import (
  "fmt"
  "log"
  "net/http"
  "e-Vote/controllers"
  "e-Vote/views"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)

var index *views.View
var formpilihan *views.View
var konfirmasi *views.View
var results []string
func main() {
  index = views.NewView("bootstrap", "views/index.gohtml")
  formpilihan = views.NewView("bootstrap", "views/formpilihan.gohtml")
  konfirmasi = views.NewView("bootstrap", "views/konfirmasi.gohtml")

  http.HandleFunc("/", indexHandler)
  http.HandleFunc("/formpilihan", formpilihanHandler)
  http.HandleFunc("/konfirmasi", konfirmasiHandler)

	port := 6969
	http.HandleFunc("/hasil/", func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
  		case "GET" :
  			s := r.URL.Path[len("/hasil/"):]
  			if s != "" {
  				controllers.GetUser(w,r,s)
  			} else {
  				controllers.GetAllUser(w,r)
  			}
      case "PATCH" : controllers.PatchPenduduk(w,r)
	  }
})
	log.Printf("Server starting on port %v\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v",port),nil))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
  index.Render(w, nil)
}

func formpilihanHandler(w http.ResponseWriter, r *http.Request) {
  nik := r.PostFormValue("username");
	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/data_voting")

	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select nik from voting WHERE nik = ?", nik)

	if err!= nil {
		log.Fatal(err)
	}

	defer db.Close()

  if checkCount(rows)==1 {
    varmap := map[string]interface{}{
    "var1": nik,
}
    formpilihan.Render(w, varmap)
  } else {
    http.Redirect(w, r, "/", 301)
  }
}
func checkCount(rows *sql.Rows) (count int) {
  i := 0
  for rows.Next() {
    	err:= rows.Scan(&count)
      if err!= nil {
    		log.Fatal(err)
    	}
      i++
    }
    if i == 1{
      log.Printf("Succes")
    } else {
      log.Printf("Maaf DPT dengan NIK yang diinput tidak terdaftar")
    }
    return i
}
func konfirmasiHandler(w http.ResponseWriter, r *http.Request) {
  nik := r.PostFormValue("nik");
  pilih := r.PostFormValue("vote");
  db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/data_voting")

	if err!= nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.Exec("UPDATE voting SET Pilihan=? WHERE nik=?", pilih, nik)

  konfirmasi.Render(w, nil)
}
